window.onload = function(){
		
	var slide01 = document.getElementById('slide01'),
		slide02 = document.getElementById('slide02'),
		slide03 = document.getElementById('slide03'),
		slide04 = document.getElementById('slide04'),
		slide05 = document.getElementById('slide05'),
		slide06 = document.getElementById('slide06'),
		slide07 = document.getElementById('slide07'),
		slide08 = document.getElementById('slide08'),
		slide09 = document.getElementById('slide09'),
		slide10 = document.getElementById('slide10'),
		slide11 = document.getElementById('slide11');
		
		
	function step01() {
	            
        var tl = new TimelineMax();
		var aTime = 1.0;
		
		tl.set(slide01, {y:-175, x:-80, autoAlpha:0.75})
		.set([slide04, slide05], {x:70})
		.set([slide07, slide08, slide09], {x:135})
		.set(slide11, {x:230, autoAlpha:1});

		tl.fromTo([slide03, slide04], aTime, {y:-50, autoAlpha:1}, {ease:Power3.easeIn, y:0})
		.fromTo(slide07, aTime, {y:50, autoAlpha:1}, {ease:Power3.easeIn, y:0}, '-=1.0')
		.to(slide04, aTime, {y:50, autoAlpha:0}, '+=1.25')
		.to(slide07, aTime, {y:-80, autoAlpha:0}, '-=1.0')
		.fromTo(slide05, aTime, {y:-50, autoAlpha:1}, {ease:Power3.easeIn, y:0}, '-=0.5')
		.fromTo(slide08, aTime, {y:50, autoAlpha:1}, {ease:Power3.easeIn, y:0}, '-=1.0')
		.to([slide03, slide05], aTime, {y:50, autoAlpha:0}, '+=1.25')
		.to(slide08, aTime, {y:-80, autoAlpha:0}, '-=1.0')
		.fromTo(slide06, aTime, {y:-50, autoAlpha:1}, {ease:Power3.easeIn, y:0}, '-=0.5')
		.fromTo(slide09, aTime, {y:50, autoAlpha:1}, {ease:Power3.easeIn, y:0}, '-=1.0')
		.to(slide06, aTime, {y:50, autoAlpha:0}, '+=1.25')
		.to(slide09, aTime, {y:-80, autoAlpha:0}, '-=1.0')
		.to(slide10, aTime, {autoAlpha:1}, '+=0.25');

	}
	
	step01();
		
};