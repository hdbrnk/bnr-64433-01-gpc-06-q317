window.onload = function(){
		
	var slide01 = document.getElementById('slide01'),
		slide02 = document.getElementById('slide02'),
		slide03 = document.getElementById('slide03'),
		slide04 = document.getElementById('slide04'),
		slide05 = document.getElementById('slide05'),
		slide06 = document.getElementById('slide06'),
		slide07 = document.getElementById('slide07'),
		slide08 = document.getElementById('slide08'),
		slide09 = document.getElementById('slide09'),
		slide10 = document.getElementById('slide10'),
		slide11 = document.getElementById('slide11'),
		slide12 = document.getElementById('slide12');
		
		
	function step01() {
	            
        var tl = new TimelineMax();
		var aTime = 1.0;
		
		tl.set(slide01, {y:-255, x:15, autoAlpha:0.5})
		.set(slide04, {x:150})
		.set([slide05, slide06], {x:267})
		.set([slide07, slide08, slide11], {x:403})
		.set(slide09, {x:578, autoAlpha:1})
		.set(slide10, {x:190})
		.set(slide12, {x:240});

		tl.fromTo([slide04, slide05], aTime, {y:-90, autoAlpha:1}, {ease:Power3.easeIn, y:0})
		.fromTo(slide07, aTime, {y:90, autoAlpha:1}, {ease:Power3.easeIn, y:-23}, '-=1.0')
		.to(slide05, aTime, {y:90, autoAlpha:0}, '+=1.25')
		.to(slide07, aTime, {y:-150, autoAlpha:0}, '-=1.0')
		.fromTo(slide06, aTime, {y:-90, autoAlpha:1}, {ease:Power3.easeIn, y:0}, '-=0.5')
		.fromTo(slide08, aTime, {y:90, autoAlpha:1}, {ease:Power3.easeIn, y:-23}, '-=1.0')
		.to([slide04, slide06], aTime, {y:90, autoAlpha:0}, '+=1.25')
		.to(slide08, aTime, {y:-150, autoAlpha:0}, '-=1.0')
		.fromTo(slide10, aTime, {y:-90, autoAlpha:1}, {ease:Power3.easeIn, y:0}, '-=0.5')
		.fromTo(slide11, aTime, {y:90, autoAlpha:1}, {ease:Power3.easeIn, y:-23}, '-=1.0')
		.to([slide10, slide06], aTime, {y:90, autoAlpha:0}, '+=1.25')
		.to(slide11, aTime, {y:-150, autoAlpha:0}, '-=1.0')
		.to(slide12, aTime, {autoAlpha:1}, '+=0.25');

	}
	
	step01();
		
};